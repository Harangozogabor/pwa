import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';



interface Pokemon{
  name: string,
  level: number,
  owner: string,
  description: string
}

@Component({
  selector: 'app-pokemons',
  templateUrl: './pokemons.component.html',
  styleUrls: ['./pokemons.component.css']
})
export class PokemonsComponent implements OnInit {

  pokemonsCollection: AngularFirestoreCollection<Pokemon>;
  pokemons: Observable<Pokemon[]>
  constructor(private afs: AngularFirestore) { }

  ngOnInit() {
    this.pokemonsCollection = this.afs.collection('pokemons', ref =>{
      return ref.orderBy('level', 'desc')
    });
    this.pokemons = this.pokemonsCollection.valueChanges();
  }

}
