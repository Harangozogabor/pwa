import { Component } from '@angular/core';
import { SwUpdate } from '@angular/service-worker'
import { interval } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';



interface Pokemon{
  name: string,
  level: number,
  owner: string,
  description: string
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pwa';
 
  constructor() { }

  ngOnInit() {
  }

}
