import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentSnapshotDoesNotExist, DocumentSnapshotExists, Action } from '@angular/fire/firestore';
import { map, take, tap } from 'rxjs/operators';
import { Observable, pipe } from 'rxjs';
import { AuthService } from '../services/auth.service';



interface Pokemon {
  name: string,
  level: number,
  owner: string,
  description: string
}


@Component({
  selector: 'app-my-pokemons',
  templateUrl: './my-pokemons.component.html',
  styleUrls: ['./my-pokemons.component.css']
})
export class MyPokemonsComponent implements OnInit {
  pokemon = {
    name: '',
    level: 0,
    owner: localStorage.getItem('email'),
    description: ''
  }
  pokemonsCollection: AngularFirestoreCollection<Pokemon>;
  pokemons: Observable<Pokemon[]>
  constructor(private afs: AngularFirestore,private authService:AuthService) { }

  ngOnInit() {
    this.pokemonsCollection = this.afs.collection('pokemons', ref => {
      return ref.orderBy('level', 'desc').where('owner', '==', localStorage.getItem('email'));
    });
    this.pokemons = this.pokemonsCollection.valueChanges();

  }

  addPokemon() {
    let shouldAdd = false;
    let description = '';
    let pokemonsRef = this.afs.collection('pokemons');
    pokemonsRef.get().toPromise()
      .then(snapshot => {
        snapshot.forEach(doc => {
          if (doc.data().name == this.pokemon.name) {
            shouldAdd = true;
            description = doc.data().description;
          }
        });
        if (shouldAdd) {
          this.pokemon.description = description;
          this.pokemonsCollection.add(this.pokemon).then(data => {
            console.log(data)
          }, err => {
            console.log(err);
          }
          );
        } else {
          this.authService.deleteUSer();
          alert('You have been banned')
        }
      })
      .catch(err => {
        console.log('Error getting documents', err);
      });

  }

}
