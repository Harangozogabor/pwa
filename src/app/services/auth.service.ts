import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private user: Observable<firebase.User>;
  private userDetails: firebase.User = null;
  constructor(private _firebaseAuth: AngularFireAuth, private router: Router) {
    this.user = _firebaseAuth.authState;
    this.user.subscribe(
      (user) => {
        console.log('emit')
        if (user) {
          this.userDetails = user;
          localStorage.setItem('email',this.userDetails.email)
        }
        else {
          this.userDetails = null;
        }
      }
    );
  }

  signInRegular(email, password) {
    const credential = firebase.auth.EmailAuthProvider.credential(email, password);
    return this._firebaseAuth.auth.signInWithEmailAndPassword(email, password);
  }


  register(email, password) {
    return this._firebaseAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  deleteUSer() {
    localStorage.removeItem('email');
    let user = firebase.auth().currentUser;
    user.delete().then( ()=> {
      return true;
    }).catch(function (error) {
      return false;
    });
    this.router.navigateByUrl('/login');
  }

  getUserEmail() {
    return  this.userDetails.email;
  }
  getUserDetails(){
    return this.userDetails;
  }
}
